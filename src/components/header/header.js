import React, { Fragment } from 'react';
import './header.scss';

export const Header = () => (
  <Fragment>
    <div className="header">
      <p>RocketBook</p>
    </div>
  </Fragment>
);
