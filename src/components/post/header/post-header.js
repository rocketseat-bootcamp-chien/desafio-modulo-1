import React from 'react';
import PropTypes from 'prop-types';
import './post-header.scss';

const PostHeader = (props) => {
  const { user, postTime } = props;
  return (
    <div className="post-header-content">
      <div className="user-photo">
        <img src={user.photo} alt="user perfil" />
      </div>
      <div className="user-data">
        <h3>{user.name}</h3>
        <p>{`${postTime.time} ${postTime.timeMarker} ago`}</p>
      </div>
    </div>
  );
};

PostHeader.propTypes = {
  user: PropTypes.object.isRequired,
  postTime: PropTypes.object.isRequired,
};

export default PostHeader;
