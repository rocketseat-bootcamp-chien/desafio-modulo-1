import React from 'react';
import PropTypes from 'prop-types';
import PostHeader from './header/post-header';
import PostContent from './content/post-content';
import './post.scss';

const Post = (props) => {
  const { post } = props;
  return (
    <div className="post-container">
      <PostHeader user={post.user} postTime={post.data.when} />
      <hr />
      <PostContent content={post.data.content} />
    </div>
  );
};

Post.propTypes = {
  post: PropTypes.object.isRequired,
};

export default Post;
