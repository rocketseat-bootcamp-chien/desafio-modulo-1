import React from 'react';
import PropTypes from 'prop-types';
import './post-content.scss';

const PostContent = (props) => {
  const { content } = props;
  return (
    <div className="post-content-data">
      <p>{content}</p>
    </div>
  );
};

PostContent.propTypes = {
  content: PropTypes.string.isRequired,
};
export default PostContent;
