import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import { Header } from './components/header/header';
import Post from './components/post/post';
import './style.scss';

class App extends Component {
  state = {
    posts: [
      {
        user: {
          name: 'Diego Lopes',
          photo:
            'http://www.acailandia.ma.gov.br/SITE2017/arquivos/secretarias/secretarios/2016/Novembro/icon-perfil_2.png',
        },
        data: {
          content: 'teste de post',
          when: {
            time: 3,
            timeMarker: 'min',
          },
        },
      },
    ],
  };

  render() {
    const { posts } = this.state;
    return (
      <Fragment>
        <Header />
        <div className="content">
          {posts.map((post, i) => (
            <Post post={post} key={i} />
          ))}
        </div>
      </Fragment>
    );
  }
}

render(<App />, document.getElementById('app'));
